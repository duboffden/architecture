package me.shpp.ddubovitsky.androidarchitecture.di;

import javax.inject.Singleton;

import dagger.Component;
import me.shpp.ddubovitsky.androidarchitecture.data.DataViewModel;

/**
 * Created by Dubovitsky Denis on 6/5/2017.
 * App component. creates app things, you know;
 */

@Singleton
@Component(modules = {NetModule.class})
public interface AppComponent {
    void inject(DataViewModel activity);
}

