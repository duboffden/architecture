package me.shpp.ddubovitsky.androidarchitecture.utils;

/**
 * Created by Dubovitsky Denis on 8/9/2017.
 */
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import me.shpp.ddubovitsky.androidarchitecture.constants.Constants;

/**
 * Listener for fast list pagination
 * Created by Katrenko Bogdan on 26.07.2016.
 */
public abstract class Endless extends RecyclerView.OnScrollListener {
    public static String TAG = "EndlessListener";
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = false; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = Constants.LOAD_THRESHOLD; // The minimum amount of items to have below your current scroll position before loading more.
    private int current_page = 1;

    private LinearLayoutManager mLinearLayoutManager;

    protected Endless(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy < 0) return;
        // check for scroll down
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        synchronized (this) {
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                loading = true;
                current_page++;
                onLoadMore(current_page);
            }
        }
    }


    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void reset() {
        loading = false;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        current_page = 1;
    }

    public abstract void onLoadMore(int current_page);
}