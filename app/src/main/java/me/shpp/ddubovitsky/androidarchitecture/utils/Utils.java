package me.shpp.ddubovitsky.androidarchitecture.utils;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import me.shpp.ddubovitsky.androidarchitecture.models.Post;

/**
 * Created by Dubovitsky Denis on 8/5/2017.
 */

public class Utils {

    public static List<Post> convertTweetsToPosts(List<Tweet> tweets) {
        ArrayList<Post> posts = new ArrayList<>();

        if (tweets == null) {
            Log.d("+++", "tere is some nulll!");
            return new ArrayList<>();
        }

        for (Tweet tweet : tweets) {
            Post post = convertTweetToPost(tweet);

            if (post != null)
                posts.add(post);
        }
        return posts;
    }

    public static Post convertTweetToPost(Tweet tweet) {

        if (tweet == null)
            return null;

        Post post = new Post();

        post.setAuthorPhotoUrl(tweet.user.name);
        post.setText(tweet.text);
        post.setAuthorPhotoUrl(tweet.user.profileImageUrl);
        post.setDate(tweet.createdAt);
        return post;
    }

    public static void setText(TextView textView, String text) {
        if (textView == null)
            return;

        if (text == null) {
            textView.setText("");
            return;
        }

        textView.setText(text);
    }

    public static void loadImage(Context context, ImageView iv, String url) {
        if (context == null)
            return;

        if (iv == null)
            return;

        if (url == null || url.isEmpty()) {
            iv.setImageBitmap(null);
            return;
        }

        Picasso.with(context).load(url).into(iv);
    }

}
