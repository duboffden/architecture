package me.shpp.ddubovitsky.androidarchitecture.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import javax.inject.Inject;

import me.shpp.ddubovitsky.androidarchitecture.ArchApp;

/**
 * Created by Dubovitsky Denis on 6/5/2017.
 */

public class DataViewModel extends ViewModel {

    private static final String TAG = DataViewModel.class.getName();

    @Inject
    UserRepository mUserRepository;

    public void init(Context context) {
        if (mUserRepository == null) {
            ((ArchApp) (context.getApplicationContext())).getAppComponent().inject(this);
        }
    }

    public LiveData<String> getData() {
        return mUserRepository.getData();
    }
}
