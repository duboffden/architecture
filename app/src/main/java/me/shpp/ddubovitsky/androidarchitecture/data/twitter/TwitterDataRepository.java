package me.shpp.ddubovitsky.androidarchitecture.data.twitter;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

import me.shpp.ddubovitsky.androidarchitecture.models.Post;
import me.shpp.ddubovitsky.androidarchitecture.models.ServerResponse;
import me.shpp.ddubovitsky.androidarchitecture.utils.Utils;
import retrofit2.Call;
import retrofit2.Response;

import static me.shpp.ddubovitsky.androidarchitecture.models.ServerResponse.Status.ERROR;
import static me.shpp.ddubovitsky.androidarchitecture.models.ServerResponse.Status.SUCCESSFUL;

/**
 * Used to work with twitter api
 * Created by Dubovitsky Denis on 8/5/2017.
 */

public class TwitterDataRepository {

    private TwitterSession mTwitterSession;
    private TwitterApiClient mTwitterApiClient;
    private long sinceId = -1;

    public TwitterDataRepository() {
        this.mTwitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        mTwitterApiClient = TwitterCore.getInstance().getApiClient(mTwitterSession);
    }


    public LiveData<ServerResponse<List<Post>>> loadMore() {
        final MutableLiveData<ServerResponse<List<Post>>> mTweets = new MutableLiveData<>();

        Long longSinceId = null;

        if (sinceId != -1)
            longSinceId = sinceId;

        Log.d("+++", "since id = " + sinceId);
        mTwitterApiClient.getStatusesService().homeTimeline(5, null, longSinceId, null, null, null, null).enqueue(new retrofit2.Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                Log.d("+++", "hmmm, response,,, hmmm,, sucessfull,,,,hmmmm");


                List<Tweet> tweets = response.body();
                if (tweets == null || tweets.isEmpty()) {
                    Log.d("+++", "empty twits");
                    return;
                }

                mTweets.setValue(new ServerResponse<>(SUCCESSFUL, Utils.convertTweetsToPosts(response.body())));
                sinceId = tweets.get(tweets.size() - 1).getId();
            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                t.printStackTrace();
                mTweets.setValue(new ServerResponse<List<Post>>(ERROR, null));
            }
        });

        return mTweets;
    }
}
