package me.shpp.ddubovitsky.androidarchitecture.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import me.shpp.ddubovitsky.androidarchitecture.models.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dubovitsky Denis on 6/5/2017.
 * Provides dummy data for test :)
 */

public class UserRepository {
    private WebService webservice;

    public UserRepository(WebService webService) {
        this.webservice = webService;
    }

    public LiveData<String> getData() {
        final MutableLiveData<String> data = new MutableLiveData<>();

        webservice.getData().enqueue(new Callback<Post>() {
            @Override
            public void onResponse(@NonNull Call<Post> call, @NonNull Response<Post> response) {
//                data.setValue(response.body().getBody());
            }

            @Override
            public void onFailure(@NonNull Call<Post> call, @NonNull Throwable t) {
                t.printStackTrace();
                Log.d("+++", "onFailure");
            }
        });

        return data;
    }
}
