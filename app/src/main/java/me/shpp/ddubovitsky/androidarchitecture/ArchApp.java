package me.shpp.ddubovitsky.androidarchitecture;

import android.app.Application;

import com.twitter.sdk.android.core.Twitter;

import me.shpp.ddubovitsky.androidarchitecture.di.AppComponent;
import me.shpp.ddubovitsky.androidarchitecture.di.DaggerAppComponent;
import me.shpp.ddubovitsky.androidarchitecture.di.NetModule;

import static java.security.AccessController.getContext;

/**
 * Created by Dubovitsky Denis on 6/18/2017.
 */

public class ArchApp extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        
        mAppComponent = DaggerAppComponent.builder().netModule(new NetModule()).build();
        Twitter.initialize(this);

    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
