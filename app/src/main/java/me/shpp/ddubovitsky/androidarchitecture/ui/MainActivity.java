package me.shpp.ddubovitsky.androidarchitecture.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import me.shpp.ddubovitsky.androidarchitecture.R;
import me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.fragments.PostsListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        goToFragment();
    }

    void goToFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment rootFragment = fragmentManager.findFragmentByTag("sometag");

        if (rootFragment == null)
            fragmentTransaction.add(R.id.rl_container, new PostsListFragment(), "sometag");
        else
            fragmentTransaction.replace(R.id.rl_container, rootFragment, "sometag");

        fragmentTransaction.commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentManager fm = getSupportFragmentManager();
        Fragment currentFragment = fm.findFragmentByTag("sometag");

        if (currentFragment != null)
            currentFragment.onActivityResult(requestCode, resultCode, data);
    }
}
