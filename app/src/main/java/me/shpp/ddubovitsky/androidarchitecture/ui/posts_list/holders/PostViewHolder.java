package me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.shpp.ddubovitsky.androidarchitecture.R;

/**
 * Created by Dubovitsky Denis on 8/5/2017.
 */

public class PostViewHolder extends RecyclerView.ViewHolder {

    TextView tvProfileName;
    ImageView ivProfilePhoto;
    TextView tvDate;
    TextView tvContent;

    public PostViewHolder(View itemView) {
        super(itemView);
        tvProfileName = itemView.findViewById(R.id.tv_profile_name);
        ivProfilePhoto = itemView.findViewById(R.id.iv_profile_image);
        tvDate = itemView.findViewById(R.id.tv_date);
        tvContent = itemView.findViewById(R.id.tv_content);
    }

    public TextView getTvProfileName() {
        return tvProfileName;
    }

    public ImageView getIvProfilePhoto() {
        return ivProfilePhoto;
    }

    public TextView getTvDate() {
        return tvDate;
    }

    public TextView getTvContent() {
        return tvContent;
    }
}
