package me.shpp.ddubovitsky.androidarchitecture.data;

import me.shpp.ddubovitsky.androidarchitecture.models.Post;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Dubovitsky Denis on 6/5/2017.
 * Our web-interface. here declared all api methods
 */

public interface WebService {
    @GET("posts/1")
    Call<Post> getData();
}