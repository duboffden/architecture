package me.shpp.ddubovitsky.androidarchitecture.ui.posts_list;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import me.shpp.ddubovitsky.androidarchitecture.data.twitter.TwitterDataRepository;
import me.shpp.ddubovitsky.androidarchitecture.models.Post;
import me.shpp.ddubovitsky.androidarchitecture.models.ServerResponse;

/**
 * Created by Dubovitsky Denis on 8/6/2017.
 */

public class PostsViewModel extends ViewModel {

    TwitterDataRepository mTwitterDataRepository;

    private MutableLiveData<List<Post>> mLivePosts = new MutableLiveData<>();
    private List<Post> mPosts;

    public void init(Context context) {
        mTwitterDataRepository = new TwitterDataRepository();
    }

    public LiveData<List<Post>> getLivePosts() {
        return mLivePosts;
    }

    public List<Post> getPosts() {
        return mPosts;
    }

    public void setPosts(List<Post> posts) {
        mPosts = posts;
    }

    public void loadPosts() {
        mTwitterDataRepository.loadMore().observeForever(new Observer<ServerResponse<List<Post>>>() {
            @Override
            public void onChanged(@Nullable ServerResponse<List<Post>> listServerResponse) {
                if (listServerResponse.isSuccessful()) {
                    mLivePosts.setValue(listServerResponse.getData());
                } else {
                    Log.d("+++", "unsuccessfull :(");
                }
            }
        });
    }
}
