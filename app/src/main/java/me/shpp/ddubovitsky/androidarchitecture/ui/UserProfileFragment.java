package me.shpp.ddubovitsky.androidarchitecture.ui;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.shpp.ddubovitsky.androidarchitecture.R;
import me.shpp.ddubovitsky.androidarchitecture.data.DataViewModel;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Dubovitsky Denis on 6/5/2017.
 */

public class UserProfileFragment extends LifecycleFragment {

    private static final String TAG = UserProfileFragment.class.getSimpleName();

    @BindView(R.id.tv_data)
    TextView tvData;
    @BindView(R.id.login_button)
    TwitterLoginButton btnTwLogin;

    DataViewModel mUserProfileViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUserProfileViewModel = ViewModelProviders.of(this).get(DataViewModel.class);
        mUserProfileViewModel.init(getContext().getApplicationContext());
        Log.d("+++", "is current active session = " + TwitterCore.getInstance().getSessionManager().getActiveSession());
        btnTwLogin.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d("+++", "okay, there is some twitter session in there :) " + result.data + " response " + result.response);
                TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterCore.getInstance().getApiClient(result.data).getStatusesService().homeTimeline(null, null, null, null, null, null, null).enqueue(new retrofit2.Callback<List<Tweet>>() {
                    @Override
                    public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                        for (Tweet t : response.body()) {
                            Log.d("+++", "tweet name = " + t.text + " author image " + t.user.profileImageUrl);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Tweet>> call, Throwable t) {

                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        btnTwLogin.onActivityResult(requestCode, resultCode, data);
    }

}