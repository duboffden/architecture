package me.shpp.ddubovitsky.androidarchitecture.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.shpp.ddubovitsky.androidarchitecture.data.UserRepository;
import me.shpp.ddubovitsky.androidarchitecture.data.WebService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dubovitsky Denis on 6/5/2017.
 * Module. just a module.
 */
@Module
public class NetModule {

    private static final String BASE_URL = "https://194.136.187.236/tournament/and_app/";

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    WebService provideWebService(Retrofit retrofit) {
        return retrofit.create(WebService.class);
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(WebService webService) {
        return new UserRepository(webService);
    }
}
