package me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.shpp.ddubovitsky.androidarchitecture.R;
import me.shpp.ddubovitsky.androidarchitecture.models.Post;
import me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.holders.PostViewHolder;

import static me.shpp.ddubovitsky.androidarchitecture.utils.Utils.loadImage;
import static me.shpp.ddubovitsky.androidarchitecture.utils.Utils.setText;

/**
 * Adapter for displaying posts
 * Created by Dubovitsky Denis on 8/5/2017.
 */

public class PostListAdapter extends RecyclerView.Adapter<PostViewHolder> {

    private List<Post> mPosts = new ArrayList<>();

    private Context mContext;

    public PostListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, null));
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        Post post = mPosts.get(position);
        setText(holder.getTvProfileName(), post.getAuthorName());
        setText(holder.getTvContent(), post.getText());
        setText(holder.getTvDate(), post.getDate());
        loadImage(mContext, holder.getIvProfilePhoto(), post.getAuthorPhotoUrl());
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public void addPosts(List<Post> posts) {
        mPosts.addAll(posts);
        Log.d("+++", "size = " + posts.size());
    }

    public void changedItemRange(int countAdded){
        notifyItemRangeChanged(mPosts.size() - countAdded, mPosts.size());
    }
}
