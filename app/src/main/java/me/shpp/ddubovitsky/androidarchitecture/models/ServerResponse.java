package me.shpp.ddubovitsky.androidarchitecture.models;

/**
 * Created by Dubovitsky Denis on 8/5/2017.
 */

public class ServerResponse<T> {

    private Status mStatus;
    private String errorCode;
    private T mData;

    public ServerResponse(Status status, T data) {
        this.mStatus = status;
        this.mData = data;
    }

    public void setStatus(Status status) {
        mStatus = status;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setData(T data) {
        mData = data;
    }

    public boolean isSuccessful() {
        return mStatus == Status.SUCCESSFUL;
    }

    public Status getStatus() {
        return mStatus;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public T getData() {
        return mData;
    }

    public enum Status {SUCCESSFUL, ERROR}
}
