package me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.fragments;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.shpp.ddubovitsky.androidarchitecture.R;
import me.shpp.ddubovitsky.androidarchitecture.models.Post;
import me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.PostsViewModel;
import me.shpp.ddubovitsky.androidarchitecture.ui.posts_list.adapters.PostListAdapter;
import me.shpp.ddubovitsky.androidarchitecture.utils.Endless;

/**
 * Created by Dubovitsky Denis on 8/5/2017.
 */

public class PostsListFragment extends LifecycleFragment {

    public static final String TAG = PostsListFragment.class.getSimpleName();

    @BindView(R.id.rv_posts)
    RecyclerView rvPosts;

    private PostsViewModel vm;
    private PostListAdapter mPostListAdapter;

    private Endless mEndless;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        vm = ViewModelProviders.of(this).get(PostsViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_posts_list, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEndless = new Endless((LinearLayoutManager) rvPosts.getLayoutManager()) {
            @Override
            public void onLoadMore(int current_page) {
                Log.d("+++", "loading more... current page = " + current_page);
                vm.loadPosts();
            }
        };

        rvPosts.addOnScrollListener(mEndless);

        mPostListAdapter = new PostListAdapter(getContext());
        rvPosts.setAdapter(mPostListAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        vm.init(getContext());
        vm.getLivePosts().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable final List<Post> listServerResponse) {
                mPostListAdapter.addPosts(listServerResponse);
                Log.d("+++", "loaded some items");
                mEndless.setLoading(false);

                if (listServerResponse == null)
                    return;

                if (listServerResponse.isEmpty())
                    return;

                rvPosts.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("+++", "size loaded = " + listServerResponse.size());
                        mPostListAdapter.changedItemRange(listServerResponse.size());
                        Log.d("+++", "so, should add some items o.o");
                    }
                });
            }
        });
        vm.loadPosts();

    }
}
